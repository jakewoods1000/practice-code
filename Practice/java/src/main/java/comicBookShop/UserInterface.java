package comicBookShop;

import java.util.List;
import java.util.Scanner;

public class UserInterface {
	
	Scanner keyboard = new Scanner(System.in);
	
	
	public int chooseStore() {
		System.out.println("Hello, please choose the store you would like to order from.");
		System.out.println("1) On Oak");
		System.out.println("2) On Elm");
		System.out.println("3) On Main");
		
		int choice = keyboard.nextInt();
		keyboard.nextLine();
		return choice;
	}


	public String menuController() {
		System.out.println("What are you looking for today?");
		System.out.println("1) A comic book");
		System.out.println("2) A TTRPG book");
		System.out.println("3) A figurine");
		System.out.println("4) Some figurine paint");
		System.out.println("5) Some dice");
		System.out.println("6) A board game");
		
		String choice; 
		int tempChoice = keyboard.nextInt();
		keyboard.nextLine();
		if(tempChoice >= 1 && tempChoice <= 6) {
			choice = String.valueOf(tempChoice);
		}
		else {
			choice = null;
		}
		
		return choice;
	}
	
	public void displayComics(List<ComicBook> listOfComics) {
		for(ComicBook comic : listOfComics) {
			System.out.printf("%-20s", "Item ID:");
			System.out.println(comic.getInventoryId());
			System.out.printf("%-20s", "Name:");
			System.out.println(comic.getName());
			System.out.printf("%-20s", "Publication:");
			System.out.println(comic.getPublication());
			System.out.printf("%-20s", "Released:");
			System.out.println(comic.getReleaseDate());
			System.out.printf("%-20s", "Price:");
			System.out.println(comic.getPrice());
			System.out.printf("%-20s", "Number in stock:");
			System.out.println(comic.getNumberInStore());
			System.out.println();
		}
		
	}

	public String buyPrompt(String item, boolean firstBook) {
		String word = (firstBook) ? "a " : "another ";
		System.out.println("Would you like to buy " + word + item + "?");
		System.out.println("Y/N");
		return keyboard.nextLine();
	}
	
	public int buyMenu(String item) {
		System.out.println("What is the item ID of the " + item + " would you like to buy?");
		int choice = keyboard.nextInt();
		keyboard.nextLine();
		return choice;
	}


	public void displayError(ErrorType error) {
		switch(error) {
		case INVALID_CHOICE: System.out.println("Invalid choice, please try again.");
		}
		
	}
}
