package comicBookShop;

import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

public class ComicBookShopApp {

	private UserInterface ui;
	
	private StoreLocation currentStore;	
	
	
	private ComicBookDAO comicBookDAO;
	
	
	
	public ComicBookShopApp() {
		ui = new UserInterface();
		
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/comicBookShop");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		comicBookDAO = new JDBCComicBookDAO(dataSource);
		
	}
	
	
	public static void main(String[] args) {
		ComicBookShopApp app = new ComicBookShopApp();
		app.run();
	}

	private void run() {
		
		
		
		boolean running = true;
		
		while(running) {
			currentStore = StoreLocation.checkValue(String.valueOf(ui.chooseStore()));
			MainMenuChoice result = MainMenuChoice.checkValue(ui.menuController());
			switch (result) {
				case COMIC_BOOK: comicBookMenuController();
				case TTRPG_BOOK: ttrpgBookMenuController();
				case FIGURINE: figurineMenuController();
				case PAINT: paintMenuController();
				case DICE: diceMenuController();
				case BOARD_GAME: boardGameMenuController();
			}
			
			ui.displayError(ErrorType.INVALID_CHOICE);	
			
		}
		
		
	}
	
	private void boardGameMenuController() {
		// TODO Auto-generated method stub
		
	}


	private void diceMenuController() {
		// TODO Auto-generated method stub
		
	}


	private void paintMenuController() {
		// TODO Auto-generated method stub
		
	}


	private void figurineMenuController() {
		// TODO Auto-generated method stub
		
	}


	private void ttrpgBookMenuController() {
		// TODO Auto-generated method stub
		
	}


	private void comicBookMenuController() {
		List<ComicBook> listOfComics = comicBookDAO.searchComicBooksInStockByStore(currentStore);
		ui.displayComics(listOfComics);
		boolean running = true;
		boolean firstBook = true;
		while(running) {
			String choice = ui.buyPrompt("comic book", firstBook);
			if(choice.equals("Y") ||choice.equals("y")) {
				long comicToBuy = ui.buyMenu("comic book");
				if(itemIsInStock(comicToBuy, listOfComics)) {
					//continue buying process here
				}
				else {
					ui.displayError(ErrorType.INVALID_CHOICE);
				}
				running = false;
			}
			
			else if(choice.equals("N") ||choice.equals("n")) {
				running = false;
			}
			else {
				ui.displayError(ErrorType.INVALID_CHOICE);
			}
		}
		
	}
	private <E extends InventoryItem> boolean itemIsInStock(long inventoryId, List<E> listOfItems) {
		boolean isOnList = false;
		for(InventoryItem item : listOfItems) {
			isOnList = (item.getInventoryId() == inventoryId) ? true : false;
		}
		return isOnList;
	}

}
