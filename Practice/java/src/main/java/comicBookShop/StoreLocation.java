package comicBookShop;

import java.util.HashMap;
import java.util.Map;

public enum StoreLocation {
	OAK ("1"),
	ELM ("2"),
	MAIN ("3");
	
	
	private final String value;
	private static Map<String, StoreLocation> map = new HashMap<>();
	
	StoreLocation(final String newValue) {
		value = newValue;
	}
	
	static {
		for (StoreLocation storelocation : StoreLocation.values()) {
			map.put(storelocation.value, storelocation);
		}
	}
	
	public static StoreLocation checkValue(String value) {
		return map.get(value);
	}
	
	public String getValue() {
		return value;
	}
	
}
