package comicBookShop;

import java.util.HashMap;
import java.util.Map;

public enum MainMenuChoice {
	
	COMIC_BOOK ("1"),
	TTRPG_BOOK ("2"),
	FIGURINE ("3"),
	PAINT ("4"),
	DICE ("5"),
	BOARD_GAME("6");
	
	
	private final String value;
	private static Map<String, MainMenuChoice> mainMenuMap = new HashMap<>();
	
	MainMenuChoice(final String newValue) {
		value = newValue;
	}
	
	static {
		for (MainMenuChoice mainMenuChoice : MainMenuChoice.values()) {
			mainMenuMap.put(mainMenuChoice.value, mainMenuChoice);
		}
	}
	
	public static MainMenuChoice checkValue(String value) {
		return mainMenuMap.get(value);
	}
	
	public String getValue() {
		return value;
	}
	
	
}
