package comicBookShop;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ComicBook extends InventoryItem{
	
	private long inventoryId;
	private long comicId;
	private String publication;
	private String artist;
	private LocalDate releaseDate;
	private String name;
	private BigDecimal price;
	private int numberInStore;
	
	
	
	public long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(long inventoryId) {
		super.setInventoryId(inventoryId);
		this.inventoryId = inventoryId;
	}
	public int getNumberInStore() {
		return numberInStore;
	}
	public void setNumberInStore(int numberInStore) {
		this.numberInStore = numberInStore;
	}
	public BigDecimal getPrice() {
		super.setPrice(price);
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		super.setItemName(name);
		this.name = name;
	}
	public long getComicId() {
		return comicId;
	}
	public void setComicId(long comicId) {
		this.comicId = comicId;
	}
	public String getPublication() {
		return publication;
	}
	public void setPublication(String publication) {
		this.publication = publication;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public LocalDate getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	
}
