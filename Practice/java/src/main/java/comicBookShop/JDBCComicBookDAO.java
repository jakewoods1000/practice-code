package comicBookShop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;



public class JDBCComicBookDAO implements ComicBookDAO {

	private JdbcTemplate jdbcTemplate;
	
	public JDBCComicBookDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	
	@Override
	public List<ComicBook> searchComicBooksInStockByStore(StoreLocation store) {
		List<ComicBook> comics = new ArrayList<>();
		String sqlSearchByStore = "SELECT * FROM comic_book ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchByStore, store);
		
		while(results.next()) {
			ComicBook currentComicbook = mapRowToComicBook(results);
			comics.add(currentComicbook);
		}
		return comics;
	}
	

	@Override
	public void updateComicBookStock(long inventoryId, int numberInStock, boolean shipmentOrdered,
			LocalDate timestamp) {
		String sqlUpdateStock = "INSERT INTO";
		jdbcTemplate.update(sqlUpdateStock);
		
	}
	
	
	
	
	private ComicBook mapRowToComicBook(SqlRowSet results) {
		ComicBook currentComicBook = new ComicBook();
		
		currentComicBook.setInventoryId(results.getLong("inventory_id"));
		currentComicBook.setComicId(results.getLong("comic_id"));
		currentComicBook.setPublication(results.getString("publication"));
		currentComicBook.setArtist(results.getString("artist"));
		currentComicBook.setReleaseDate(results.getDate("release_date").toLocalDate()); // wrap in if statement to make sure release_date isn't null
		currentComicBook.setName(results.getString("name"));
		currentComicBook.setPrice(results.getBigDecimal("price")); //price needs cast in to decimal in the database
		currentComicBook.setNumberInStore(results.getInt("number_in_store"));
		
		return currentComicBook;
	}


	

}
