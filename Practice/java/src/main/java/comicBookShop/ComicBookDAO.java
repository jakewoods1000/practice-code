package comicBookShop;

import java.time.LocalDate;
import java.util.List;

public interface ComicBookDAO {
	
	
	/**
	 * Get all comic books in stock from datastore by store
	 * 
	 * @param store to search
	 * @return all matching comic books as ComicBook objects in a List
	 */
	public List<ComicBook> searchComicBooksInStockByStore(StoreLocation store);
	
	/**
	 * Insert new stock number for a specific comic book after it was bought
	 * 
	 * @param comic book InventoryID, new number in stock, whether a shipment will be ordered, timestamp
	 * @return nothing?
	 */
	public void updateComicBookStock(long inventoryId, int numberInStock, boolean shipmentOrdered, LocalDate timestamp);
}
