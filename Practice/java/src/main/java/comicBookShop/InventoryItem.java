package comicBookShop;

import java.math.BigDecimal;

public abstract class InventoryItem {
	private long inventoryId;
	private String itemName;
	private BigDecimal price;
	
	
	


	public long getInventoryId() {
		return inventoryId;
	}


	public void setInventoryId(long inventoryId) {
		this.inventoryId = inventoryId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
}
