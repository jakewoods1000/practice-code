package com.techelevator.ticTacToe;

import java.util.Scanner;

public class UserInterface {
	
	Scanner keyboard = new Scanner(System.in);
	
	public String welcomePlayer1() {
		System.out.println("Hello players.\n"
				+ "Player 1 please enter your name.");
		return keyboard.nextLine();
	}
	public String welcomePlayer2() {
		System.out.println("Player 2 please enter your name.");
		return keyboard.nextLine();
	}
	public int promptTurn(String name, String mark) {
		System.out.println(name + ", where would you like to place your " + mark + "?");
		int choice = keyboard.nextInt();
		keyboard.nextLine();
		
		return choice;
	}
	public void displayWinner(String winner) {
		System.out.println("Congrats " + winner + " you won!");
	}
}
