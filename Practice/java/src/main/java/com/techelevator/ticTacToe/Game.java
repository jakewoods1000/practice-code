package com.techelevator.ticTacToe;

import java.util.Random;

public class Game {
	
	private boolean gameRun = true;
	
	Random rand = new Random();
	
	GameRules thisGame = new GameRules();
	
	UserInterface menu = new UserInterface();
	
	Player player1 = new Player();
	Player player2 = new Player();
	
	boolean randomStart = rand.nextBoolean();
	boolean randomMark = rand.nextBoolean();
	
	String[] spots = {" ", " ", " ", " ", " ", " ", " ", " ", " "};
	
	int choice = 0;
	
	String lastPlayed;
	
	boolean choiceAccepted;
	
	
	
	public void play() {
		
		
		player1.setName(menu.welcomePlayer1());
		player2.setName(menu.welcomePlayer2());
		player1.setIsTurn(randomStart);
		player2.setIsTurn(!randomStart);
		player1.setxOrO(randomMark);
		player2.setxOrO(!randomMark);
		
		while(gameRun) {
			gameBoard();
			choiceAccepted = false;
			if(player1.isTurn()) {
				while(!choiceAccepted) {
					choice = menu.promptTurn(player1.getName(), player1.getxOrO());
					checkChoice(choice, player1.getxOrO());
					player1.setIsTurn(!player1.isTurn());
					player2.setIsTurn(!player2.isTurn());
					lastPlayed = player1.getName();
				}
			}
			else {
				while(!choiceAccepted) {
					choice = menu.promptTurn(player2.getName(), player2.getxOrO());
					checkChoice(choice, player2.getxOrO());
					player2.setIsTurn(!player2.isTurn());
					player1.setIsTurn(!player1.isTurn());
					lastPlayed = player2.getName();
				}
			}
			gameRules();
			emptySpotsLeft();
		}
	}
	
	public void gameRules() {
		if((spots[0] == spots[1] && spots[1] == spots[2]) && spots[1] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[3] == spots[4] && spots[4] == spots[5]) && spots[4] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[6] == spots[7] && spots[7] == spots[8]) && spots[7] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[0] == spots[3] && spots[3] == spots[6]) && spots[3] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[1] == spots[4] && spots[4] == spots[7]) && spots[4] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[2] == spots[5] && spots[5] == spots[8]) && spots[5] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[6] == spots[4] && spots[4] == spots[2]) && spots[4] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
		else if((spots[0] == spots[4] && spots[4] == spots[8]) && spots[4] != " ") {
			gameBoard();
			menu.displayWinner(lastPlayed);
			gameRun = false;
		}
	}
	
	public void emptySpotsLeft() {
		int i;
		for(i = 0; i < spots.length; i++) {
			if (spots[i] == " ") {
				break;
			}
		}
		if(!gameRun) {
			
		}
		else if (i == 9){
			gameBoard();
			System.out.println("You're both losers!");
			gameRun = false;
		}
	}
	
	public void gameBoard() {
		System.out.println("   |   |   " + "      " + "   |   |   "); 
		System.out.println(" 7 | 8 | 9 " + "      " + " " + spots[6] +  " | " + spots[7] + " | " + spots[8] + " "); 
		System.out.println("___|___|___" + "      " + "___|___|___"); 
		System.out.println("   |   |   " + "      " + "   |   |   "); 
		System.out.println(" 4 | 5 | 6 " + "      " + " " + spots[3] +  " | " + spots[4] + " | " + spots[5] + " ");
		System.out.println("___|___|___" + "      " + "___|___|___"); 
		System.out.println("   |   |   " + "      " + "   |   |   "); 
		System.out.println(" 1 | 2 | 3 " + "      " + " " + spots[0] +  " | " + spots[1] + " | " + spots[2] + " "); 
		System.out.println("   |   |   " + "      " + "   |   |   "); 
	}
	
	public void checkChoice(int choice, String mark) {
		
		for(int i = 0; i < spots.length; i++) {
			if(spots[i] == " " && choice == i + 1) {
				spots[i] = mark;
				choiceAccepted = true;
			}
		}
		if(choiceAccepted) {
			
		}
		else {
			System.out.println("Pick a number 1-9, that has not already been chosen");
			gameBoard();
		}
	}

	
	
}
