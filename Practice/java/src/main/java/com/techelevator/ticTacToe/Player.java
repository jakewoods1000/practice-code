package com.techelevator.ticTacToe;

public class Player {
	
	private String name;
	private boolean isTurn;
	private String xOrO;

	public String getxOrO() {
		return xOrO;
	}

	public void setxOrO(boolean randomMark) {
		if(randomMark) {
			xOrO = "X";
		}
		else {
			xOrO = "O";
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isTurn() {
		return isTurn;
	}

	public void setIsTurn(boolean isTurn) {
		this.isTurn = isTurn;
	}
	
	
}
