package com.planarforge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class PlanarForgeApp {

	public static void main(String[] args) {
		SpringApplication.run(PlanarForgeApp.class, args);

	}

}
