package com.planarforge.controllers;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planarforge.daos.CreatureDAO;
import com.planarforge.models.Creature;

@PreAuthorize("isAuthenticated()")
@RestController
public class CreatureController {
	private CreatureDAO creatureDAO;
	
	public CreatureController(CreatureDAO creatureDAO) {
		this.creatureDAO = creatureDAO;
	}
	
	@RequestMapping (path = "/creatures/officialCreatures", method = RequestMethod.GET)
	public List<Creature> fetchOfficialCreatures() {
		
		return creatureDAO.fetchOfficialCreatures();
	}
	
	@RequestMapping (path = "/creatures/homebrewCreatures/{userId}", method = RequestMethod.GET)
	public List<Creature> fetchHomebrewCreatures(@PathVariable UUID userId) {
		
		return creatureDAO.fetchHomebrewCreatures(userId, false);
	}
	
	@RequestMapping (path = "/creatures/allCreatures/{userId}", method = RequestMethod.GET)
	public List<Creature> fetchAllCreatures(@PathVariable UUID userId) {
		
		return creatureDAO.fetchHomebrewCreatures(userId, true);
	}
	
	@RequestMapping (path = "/creatures/{creatureId}", method = RequestMethod.GET)
	public Creature fetchCreatureByID(@PathVariable UUID creatureId, Principal principal) {
		return creatureDAO.fetchCreatureByID(creatureId, principal.getName());
		
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping (path = "/creatures", method = RequestMethod.POST)
	public Creature createCreature(@RequestBody Creature newCreature, Principal principal) {
		return creatureDAO.createCreature(newCreature, principal.getName());
			
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping (path = "/creatures/{creatureId}", method = RequestMethod.PUT)
	public Creature updateCreature(@PathVariable UUID creatureId, @RequestBody Creature creature, Principal principal) {
		return creatureDAO.updateCreature(creatureId, creature, principal.getName());
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping (path = "/creatures/{creatureId}", method = RequestMethod.DELETE)
	public void deleteCreature(@PathVariable UUID creatureId, Principal principal) {
		creatureDAO.deleteCreature(creatureId, principal.getName());
	}
}
