package com.planarforge.daos;

import java.util.List;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.planarforge.models.Creature;

@Service
public class CreatureSqlDAO implements CreatureDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	public CreatureSqlDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<Creature> fetchOfficialCreatures() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Creature> fetchHomebrewCreatures(UUID userId, boolean includeOfficial) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Creature fetchCreatureByID(UUID creatureId, String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Creature createCreature(Creature newCreature, String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Creature updateCreature(UUID creatureId, Creature creature, String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteCreature(UUID creatureId, String username) {
		// TODO Auto-generated method stub
		
	}

	

}
