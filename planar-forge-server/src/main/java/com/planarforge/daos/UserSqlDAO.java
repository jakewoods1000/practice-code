package com.planarforge.daos;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.planarforge.models.User;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class UserSqlDAO implements UserDAO {

   
    private JdbcTemplate jdbcTemplate;

    public UserSqlDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public UUID findIdByUsername(String username) {
        
    	return jdbcTemplate.queryForObject("select user_id from users where username = ?", UUID.class, username);
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        String sql = "select * from users";

        SqlRowSet results = jdbcTemplate.queryForRowSet(sql);
        while(results.next()) {
            User user = mapRowToUser(results);
            users.add(user);
        }

        return users;
    }

    @Override
    public User findByUsername(String username) throws UsernameNotFoundException {
        for (User user : this.findAll()) {
            if( user.getUsername().toLowerCase().equals(username.toLowerCase())) {
                return user;
            }
        }
        throw new UsernameNotFoundException("User " + username + " was not found.");
    }

    @Override
    public void create(String username, String password) {
        
        UUID newUserId = UUID.randomUUID();
        // create user
        
        String insertUserSql = "INSERT INTO user (user_id, username, password_hash) VALUES (?, ?, ?)";
        String passwordHash = new BCryptPasswordEncoder().encode(password);
        
        jdbcTemplate.update(insertUserSql, newUserId, username, passwordHash);
        
        
    }

    private User mapRowToUser(SqlRowSet rs) {
        User user = new User();
        user.setId(UUID.fromString(rs.getString("user_id")));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password_hash"));
        user.setActivated(true);
        user.setAuthorities("ROLE_USER");
        return user;
    }
}
