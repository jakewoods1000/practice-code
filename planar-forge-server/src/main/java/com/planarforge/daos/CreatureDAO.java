package com.planarforge.daos;

import java.util.List;
import java.util.UUID;

import com.planarforge.models.Creature;

public interface CreatureDAO {
	
	public List<Creature> fetchOfficialCreatures();
	
	public List<Creature> fetchHomebrewCreatures(UUID userId, boolean includeOfficial);
	
	
	public Creature fetchCreatureByID(UUID creatureId, String username);
	
	public Creature createCreature(Creature newCreature, String username);
	
	public Creature updateCreature(UUID creatureId, Creature creature, String username);
	
	public void deleteCreature(UUID creatureId, String username);
}
