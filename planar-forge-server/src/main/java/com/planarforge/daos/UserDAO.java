package com.planarforge.daos;

import java.util.List;
import java.util.UUID;

import com.planarforge.models.User;


public interface UserDAO {
	List<User> findAll();

    User findByUsername(String username);

    UUID findIdByUsername(String username);

    void create(String username, String password);
}

