package com.planarforge.models;

import java.util.UUID;

public class Creature {
	private UUID creatureid;
	private String username;
	private String name;
	private Size size; 
	private String type;
	private String alignment;
	private int proficiencyBonus;
	private int armorClass;
	private String armorClassDescriptor;
	private int hitPoints;
	private String speed;
	private Stats stats;
	private SavingThrows savingThrows;
	private Skills skills;
	private String senses;
	private String challengRating;
	private String languages;
	private String damageVulnerabilities;
	private String damageResistances;
	private String damageImmunities;
	private String conditionImmunities;
	private String legendaryActionsHeader;
	private String creatureDescriptor;
	
	
	public UUID getCreatureid() {
		return creatureid;
	}
	public void setCreatureid(UUID creatureid) {
		this.creatureid = creatureid;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Size getSize() {
		return size;
	}
	public void setSize(Size size) {
		this.size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAlignment() {
		return alignment;
	}
	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}
	public int getProficiencyBonus() {
		return proficiencyBonus;
	}
	public void setProficiencyBonus(int proficiencyBonus) {
		this.proficiencyBonus = proficiencyBonus;
	}
	public int getArmorClass() {
		return armorClass;
	}
	public void setArmorClass(int armorClass) {
		this.armorClass = armorClass;
	}
	public String getArmorClassDescriptor() {
		return armorClassDescriptor;
	}
	public void setArmorClassDescriptor(String armorClassDescriptor) {
		this.armorClassDescriptor = armorClassDescriptor;
	}
	public int getHitPoints() {
		return hitPoints;
	}
	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public Stats getStats() {
		return stats;
	}
	public void setStats(Stats stats) {
		this.stats = stats;
	}
	public SavingThrows getSavingThrows() {
		return savingThrows;
	}
	public void setSavingThrows(SavingThrows savingThrows) {
		this.savingThrows = savingThrows;
	}
	public Skills getSkills() {
		return skills;
	}
	public void setSkills(Skills skills) {
		this.skills = skills;
	}
	public String getSenses() {
		return senses;
	}
	public void setSenses(String senses) {
		this.senses = senses;
	}
	public String getChallengRating() {
		return challengRating;
	}
	public void setChallengRating(String challengRating) {
		this.challengRating = challengRating;
	}
	public String getLanguages() {
		return languages;
	}
	public void setLanguages(String languages) {
		this.languages = languages;
	}
	public String getDamageVulnerabilities() {
		return damageVulnerabilities;
	}
	public void setDamageVulnerabilities(String damageVulnerabilities) {
		this.damageVulnerabilities = damageVulnerabilities;
	}
	public String getDamageResistances() {
		return damageResistances;
	}
	public void setDamageResistances(String damageResistances) {
		this.damageResistances = damageResistances;
	}
	public String getDamageImmunities() {
		return damageImmunities;
	}
	public void setDamageImmunities(String damageImmunities) {
		this.damageImmunities = damageImmunities;
	}
	public String getConditionImmunities() {
		return conditionImmunities;
	}
	public void setConditionImmunities(String conditionImmunities) {
		this.conditionImmunities = conditionImmunities;
	}
	public String getLegendaryActionsHeader() {
		return legendaryActionsHeader;
	}
	public void setLegendaryActionsHeader(String legendaryActionsHeader) {
		this.legendaryActionsHeader = legendaryActionsHeader;
	}
	public String getCreatureDescriptor() {
		return creatureDescriptor;
	}
	public void setCreatureDescriptor(String creatureDescriptor) {
		this.creatureDescriptor = creatureDescriptor;
	}

	
}
