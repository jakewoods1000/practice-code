package com.planarforge.models;

public class Stats {
	private int strScore;
	private int strMod;
	private int dexScore;
	private int dexMod;
	private int conScore;
	private int conMod;
	private int intScore;
	private int intMod;
	private int wisScore;
	private int wisMod;
	private int chaScore;
	private int chaMod;
	
	
	public int getStrScore() {
		return strScore;
	}
	public void setStrScore(int strScore) {
		this.strScore = strScore;
	}
	public int getStrMod() {
		return strMod;
	}
	public void setStrMod(int strMod) {
		this.strMod = strMod;
	}
	public int getDexScore() {
		return dexScore;
	}
	public void setDexScore(int dexScore) {
		this.dexScore = dexScore;
	}
	public int getDexMod() {
		return dexMod;
	}
	public void setDexMod(int dexMod) {
		this.dexMod = dexMod;
	}
	public int getConScore() {
		return conScore;
	}
	public void setConScore(int conScore) {
		this.conScore = conScore;
	}
	public int getConMod() {
		return conMod;
	}
	public void setConMod(int conMod) {
		this.conMod = conMod;
	}
	public int getIntScore() {
		return intScore;
	}
	public void setIntScore(int intScore) {
		this.intScore = intScore;
	}
	public int getIntMod() {
		return intMod;
	}
	public void setIntMod(int intMod) {
		this.intMod = intMod;
	}
	public int getWisScore() {
		return wisScore;
	}
	public void setWisScore(int wisScore) {
		this.wisScore = wisScore;
	}
	public int getWisMod() {
		return wisMod;
	}
	public void setWisMod(int wisMod) {
		this.wisMod = wisMod;
	}
	public int getChaScore() {
		return chaScore;
	}
	public void setChaScore(int chaScore) {
		this.chaScore = chaScore;
	}
	public int getChaMod() {
		return chaMod;
	}
	public void setChaMod(int chaMod) {
		this.chaMod = chaMod;
	}
	
	
}
