package com.planarforge.models;

public enum Size {
	TINY,
	SMALL,
	MEDIUM,
	LARGE,
	HUGE,
	GARGANTUAN
}
