package com.planarforge.models;

public class SavingThrows {
	private int strSave;
	private int dexSave;
	private int conSave;
	private int intSave;
	private int wisSave;
	private int chaSave;
	
	
	public int getStrSave() {
		return strSave;
	}
	public void setStrSave(int strSave) {
		this.strSave = strSave;
	}
	public int getDexSave() {
		return dexSave;
	}
	public void setDexSave(int dexSave) {
		this.dexSave = dexSave;
	}
	public int getConSave() {
		return conSave;
	}
	public void setConSave(int conSave) {
		this.conSave = conSave;
	}
	public int getIntSave() {
		return intSave;
	}
	public void setIntSave(int intSave) {
		this.intSave = intSave;
	}
	public int getWisSave() {
		return wisSave;
	}
	public void setWisSave(int wisSave) {
		this.wisSave = wisSave;
	}
	public int getChaSave() {
		return chaSave;
	}
	public void setChaSave(int chaSave) {
		this.chaSave = chaSave;
	}
	
	
}
