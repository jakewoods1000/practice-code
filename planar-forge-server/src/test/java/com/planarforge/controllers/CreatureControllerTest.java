package com.planarforge.controllers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.planarforge.daos.CreatureSqlDAO;
import com.planarforge.models.Creature;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
public class CreatureControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CreatureSqlDAO mockCreatureSqlDAO;
	
	Creature creature1 ;
	
	ObjectMapper objectMapper;
	
	
	List<Creature> expectedList;
	
	UUID expectedUuid1;
	
	UUID userUuid;

	@BeforeEach
	public void setUp() {
		expectedUuid1 = UUID.randomUUID();
		userUuid = UUID.randomUUID();
		expectedList = new ArrayList<>();
		
		creature1 = new Creature();
	    creature1.setUsername("testuser");
	    creature1.setCreatureid(expectedUuid1);
	    creature1.setName("test monster");
	    expectedList.add(creature1);
	    
	    Creature creature2 = new Creature();
	    UUID expectedUuid2 = UUID.randomUUID();
	    creature2.setUsername("testuser");
	    creature2.setCreatureid(expectedUuid2);
	    creature2.setName("test monster2");
	    expectedList.add(creature2);
	    
	    
	    Creature creature3 = new Creature();
	    UUID expectedUuid3 = UUID.randomUUID();
	    creature3.setUsername("testuser");
	    creature3.setCreatureid(expectedUuid3);
	    creature3.setName("test monster3");
	    expectedList.add(creature3);
	    
	    objectMapper = new ObjectMapper();
	    
	}
	
	
	@Test
	@WithMockUser(username = "testuser")
	public void fetchCreatureByIDSuccessTest() throws Exception {
		
	    
	    
		Mockito.when(mockCreatureSqlDAO.fetchCreatureByID(expectedUuid1, "testuser")).thenReturn(creature1);
		
		MvcResult result = mockMvc.perform(get("/creatures/{id}", expectedUuid1))
		 	.andExpect(status().isOk())
		 	.andReturn();
		
		Creature returnedCreature = objectMapper.readValue(result.getResponse().getContentAsString(), Creature.class);
		
		assertEquals("test monster", returnedCreature.getName());
		
		Mockito.verify(mockCreatureSqlDAO).fetchCreatureByID(expectedUuid1, "testuser");
	}
	
	@Test
	@WithMockUser(username = "testuser")
	public void deleteCreatureTest() throws Exception {
		
		
		Mockito.doNothing().when(mockCreatureSqlDAO).deleteCreature(expectedUuid1, "testuser");
		
		mockMvc.perform(delete("/creatures/{id}", expectedUuid1)).andExpect(status().isNoContent());
		 	
		Mockito.verify(mockCreatureSqlDAO).deleteCreature(expectedUuid1, "testuser");
	
	}
	
	@Test
	@WithMockUser(username = "testuser")
	public void fetchPublicCreaturesTest() throws Exception {
		
		Mockito.when(mockCreatureSqlDAO.fetchOfficialCreatures()).thenReturn(expectedList);
		
		MvcResult result = mockMvc.perform(get("/creatures/officialCreatures"))
			.andExpect(status().isOk())
			.andReturn();
		
		List<Creature> returnedList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Creature>>(){});
		
		assertEquals(expectedList.size(), returnedList.size());
		
		Mockito.verify(mockCreatureSqlDAO).fetchOfficialCreatures();
		
			
	}
	
	@Test
	@WithMockUser(username = "testuser")
	public void fetchHomebrewCreaturesTest() throws Exception {
		
		Mockito.when(mockCreatureSqlDAO.fetchHomebrewCreatures(userUuid, false)).thenReturn(expectedList);
		
		MvcResult result = mockMvc.perform(get("/creatures/homebrewCreatures/{userId}", userUuid))
			.andExpect(status().isOk())
			.andReturn();
		
		List<Creature> returnedList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Creature>>(){});
		
		assertEquals(expectedList.size(), returnedList.size());
		
		Mockito.verify(mockCreatureSqlDAO).fetchHomebrewCreatures(userUuid, false);
		
			
	}
	
	@Test
	@WithMockUser(username = "testuser")
	public void fetchAllCreaturesTest() throws Exception {
		
		Mockito.when(mockCreatureSqlDAO.fetchHomebrewCreatures(userUuid, true)).thenReturn(expectedList);
		
		MvcResult result = mockMvc.perform(get("/creatures/allCreatures/{userId}", userUuid))
			.andExpect(status().isOk())
			.andReturn();
		
		List<Creature> returnedList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Creature>>(){});
		
		assertEquals(expectedList.size(), returnedList.size());
		
		Mockito.verify(mockCreatureSqlDAO).fetchHomebrewCreatures(userUuid, true);
		
			
	}
	
	@Test
	@WithMockUser(username = "testuser")
	public void createCreatureTest() throws Exception {
		
		Mockito.when(mockCreatureSqlDAO.createCreature(ArgumentMatchers.any(),ArgumentMatchers.anyString())).thenReturn(creature1);
		
	    String requestJson = objectMapper.writeValueAsString(creature1);
		
		MvcResult result = mockMvc.perform(post("/creatures").contentType(MediaType.APPLICATION_JSON).content(requestJson))
			.andExpect(status().isCreated())
			.andReturn();
		
		Creature returnedCreature = objectMapper.readValue(result.getResponse().getContentAsString(), Creature.class);
		
		assertEquals(expectedUuid1, returnedCreature.getCreatureid());

			
	}
	
	@Test
	@WithMockUser(username = "testuser")
	public void updateCreatureTest() throws Exception {
		
		Mockito.when(mockCreatureSqlDAO.updateCreature(ArgumentMatchers.any(), ArgumentMatchers.any(),ArgumentMatchers.anyString())).thenReturn(creature1);
		
	    String requestJson = objectMapper.writeValueAsString(creature1);
		
		MvcResult result = mockMvc.perform(put("/creatures/{creatureId}", expectedUuid1).contentType(MediaType.APPLICATION_JSON).content(requestJson))
			.andExpect(status().isNoContent())
			.andReturn();
		
		Creature returnedCreature = objectMapper.readValue(result.getResponse().getContentAsString(), Creature.class);
		
		assertEquals(expectedUuid1, returnedCreature.getCreatureid());

			
	}
	
}





