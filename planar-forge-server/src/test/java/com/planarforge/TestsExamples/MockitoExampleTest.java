package com.planarforge.TestsExamples;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import java.security.Principal;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.planarforge.controllers.CreatureController;
import com.planarforge.daos.CreatureSqlDAO;
import com.planarforge.models.Creature;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class MockitoExampleTest {

	@Spy
	private Creature spiedCreature = new Creature();
	
	@Mock
	private CreatureSqlDAO mockedCreatureDAO;
	
	@Mock
	private Principal mockedPrincipal;
	
	private CreatureController controller;
	
	UUID expectedUuid;
	
	@BeforeEach
	public void setUp() throws Exception {
		
		controller = new CreatureController(mockedCreatureDAO);
		expectedUuid = UUID.randomUUID();
		
	}
	


	
	@Test
	public void fetchCreatureByIdTestFound() throws Exception {
		
		spiedCreature.setUsername("test user");
		spiedCreature.setCreatureid(expectedUuid);
		spiedCreature.setName("test monster");
		
		Mockito.when(mockedCreatureDAO.fetchCreatureByID(expectedUuid, "test user")).thenReturn(spiedCreature);
		Mockito.when(mockedPrincipal.getName()).thenReturn("test user");
		//try mocking a principal instead of using a real one to test
		assertNotNull(controller.fetchCreatureByID(expectedUuid, mockedPrincipal));
		assertEquals("test monster", controller.fetchCreatureByID(expectedUuid, mockedPrincipal).getName());
		

		
	}
	
	

}
