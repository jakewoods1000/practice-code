package com.planarforge.daos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.planarforge.models.Creature;
import com.planarforge.models.SavingThrows;
import com.planarforge.models.Size;
import com.planarforge.models.Skills;
import com.planarforge.models.Stats;

class CreatureDAOTest extends DAOIntegrationTest {
	
	private UUID TEST_USER_ID1;
	
	private UUID TEST_USER_ID2;
	
	private Creature testMonster1;
	private Creature testMonster2;
	private Creature testMOnster3;
	
	private Stats testStats1;
	private Stats testStats2;
	private Stats testStats3;
	
	private SavingThrows testSavingThrows1;
	private SavingThrows testSavingThrows2;
	private SavingThrows testSavingThrows3;
	
	private Skills testSkills1;
	private Skills testSkills2;
	private Skills testSkills3;
	
	private JdbcTemplate jdbcTemplate;
	private CreatureSqlDAO creatureDAO;
	private UserSqlDAO userDAO;

	@BeforeEach
	public void setUp() throws Exception {
		jdbcTemplate = new JdbcTemplate(this.getDataSource());
		creatureDAO = new CreatureSqlDAO(jdbcTemplate);
		userDAO = new UserSqlDAO(jdbcTemplate);
		
		userDAO.create("Test User 1", "password1");
		TEST_USER_ID1 = userDAO.findIdByUsername("Test User 1");
		
		userDAO.create("Test User 2", "password2");
		TEST_USER_ID2 = userDAO.findIdByUsername("Test User 2");
		
		testStats1 = new Stats();
		testStats1.setStrScore(11);
		testStats1.setDexScore(11);
		testStats1.setConScore(11);
		testStats1.setIntScore(11);
		testStats1.setWisScore(11);
		testStats1.setChaScore(11);
		
		testSavingThrows1 = new SavingThrows();
		testSavingThrows1.setStrSave(2);
		testSavingThrows1.setDexSave(-1);
		testSavingThrows1.setConSave(5);
		testSavingThrows1.setIntSave(3);
		testSavingThrows1.setWisSave(-4);
		testSavingThrows1.setChaSave(3);
		
		testSkills1 = new Skills();
		testSkills1.setAcrobatics(0);
		testSkills1.setSurvival(3);
		testSkills1.setIntimidation(-4);
		
		testMonster1 = new Creature();
		testMonster1.setUsername("Test User 1");
		testMonster1.setName("Test Monster 1");
		testMonster1.setSize(Size.LARGE);
		testMonster1.setType("Test Type 1");
		testMonster1.setAlignment("Test Alignment");
		testMonster1.setProficiencyBonus(2);
		testMonster1.setArmorClass(15);
		testMonster1.setArmorClassDescriptor("Natural Armor");
		testMonster1.setHitPoints(15);
		testMonster1.setSpeed("30 ft.");
		testMonster1.setStats(testStats1);
		testMonster1.setSavingThrows(testSavingThrows1);
		testMonster1.setSkills(testSkills1);
		testMonster1.setSenses("Darkvision");
		testMonster1.setChallengRating("1/2 10XP");
		testMonster1.setLanguages("Common, Java");
		testMonster1.setDamageVulnerabilities("Incredibly long test setups");
		testMonster1.setDamageResistances(null);
		testMonster1.setDamageImmunities(null);
		
	}

	

	@Test
	public void fetchOfficialCreaturesTest() throws Exception {
		fail("Not yet implemented");
	}

}
