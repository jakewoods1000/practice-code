package com.planarforge.daos;


import javax.sql.DataSource;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

import org.springframework.jdbc.datasource.SingleConnectionDataSource;

class DAOIntegrationTest {

	private static SingleConnectionDataSource dataSource;
	
	
	
	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/planarforge");
		dataSource.setUsername("testUser");
		dataSource.setPassword("postgres");
		
		dataSource.setAutoCommit(false);
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
		dataSource.destroy();
	}


	@AfterEach
	void tearDown() throws Exception {
		dataSource.getConnection().rollback();
	}

	protected DataSource getDataSource() {
		return dataSource;
	}

}
