START TRANSACTION;

CREATE TABLE users (
        user_id SERIAL,
        password_hash VARCHAR (128) NOT NULL,
        CONSTRAINT pk_user_id PRIMARY KEY (user_id)
);

CREATE TABLE types (
     type_id SERIAL,
     type_name VARCHAR (128) NOT NULL,
     user_id integer NOT NULL,
     CONSTRAINT pk_type_id PRIMARY KEY (type_id),
     CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id) 
);

CREATE TABLE creatures (
        creature_id SERIAL,
        user_id integer NOT NULL,
        name VARCHAR (128) NOT NULL,
        size VARCHAR (32) NOT NULL,
        type_id integer NOT NULL,
        alignment VARCHAR (32),
        armor_class integer NOT NULL,
        armor_class_descriptor VARCHAR (64),
        hit_points integer NOT NULL,
        hit_dice VARCHAR (32) NOT NULL,
        speed VARCHAR (264) NOT NULL,
        stats JSON NOT NULL,
        saving_throws JSON,
        skills JSON,
        senses VARCHAR (264) NOT NULL,
        challenge_rating VARCHAR (32),
        languages VARCHAR (264),
        damage_vulnerabilities VARCHAR (264),
        damage_resistances VARCHAR (264),
        damage_immunities VARCHAR (264),
        condition_immunities VARCHAR (264),
        legendary_actions_header VARCHAR (528),
        creature_descriptor VARCHAR (1056),
        CONSTRAINT pk_creature_id PRIMARY KEY (creature_id),
        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id),
        CONSTRAINT fk_type_id FOREIGN KEY (type_id) REFERENCES types(type_id)
);

CREATE TABLE subtypes (
        subtype_id SERIAL,
        user_id integer NOT NULL,
        subtype_name VARCHAR (32) NOT NULL,
        CONSTRAINT pk_subtype_id PRIMARY KEY (subtype_id),
        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id)
        
);

CREATE TABLE creatures_subtypes (
        creature_id integer,
        subtype_id integer,
        CONSTRAINT fk_creature_id FOREIGN KEY (creature_id) REFERENCES creatures(creature_id),
        CONSTRAINT fk_subtype_id FOREIGN KEY (subtype_id) REFERENCES subtypes(subtype_id)
);

CREATE TABLE attributes (
        attribute_id serial,
        user_id integer NOT NULL,
        attribute_name VARCHAR (32) NOT NULL,
        attribute_descriptor VARCHAR (1056) NOT NULL,
        CONSTRAINT pk_attribute_id PRIMARY KEY (attribute_id),
        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE creatures_attributes (
        creature_id integer,
        attribute_id integer,
        CONSTRAINT fk_creature_id FOREIGN KEY (creature_id) REFERENCES creatures(creature_id),
        CONSTRAINT fk_attribute_id FOREIGN KEY (attribute_id) REFERENCES attributes(attribute_id)
);

CREATE TABLE actions (
        action_id serial,
        user_id integer NOT NULL,
        action_name VARCHAR (32) NOT NULL,
        action_descriptor VARCHAR (1056) NOT NULL,
        CONSTRAINT pk_action_id PRIMARY KEY (action_id),
        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE creatures_actions (
        creature_id integer,
        action_id integer,
        CONSTRAINT fk_creature_id FOREIGN KEY (creature_id) REFERENCES creatures(creature_id),
        CONSTRAINT fk_action_id FOREIGN KEY (action_id) REFERENCES actions(action_id)
);

CREATE TABLE reactions (
        reaction_id serial,
        user_id integer NOT NULL,
        reaction_name VARCHAR (32) NOT NULL,
        reaction_descriptor VARCHAR (1056) NOT NULL,
        CONSTRAINT pk_reaction_id PRIMARY KEY (reaction_id),
        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE creatures_reactions (
        creature_id integer,
        reaction_id integer,
        CONSTRAINT fk_creature_id FOREIGN KEY (creature_id) REFERENCES creatures(creature_id),
        CONSTRAINT fk_reaction_id FOREIGN KEY (reaction_id) REFERENCES reactions(reaction_id)
);

CREATE TABLE legendary_actions (
        legendary_action_id serial,
        user_id integer NOT NULL,
        legendary_action_name VARCHAR (32) NOT NULL,
        legendary_action_descriptor VARCHAR (1056) NOT NULL,
        CONSTRAINT pk_legendary_action_id PRIMARY KEY (legendary_action_id),
        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE creatures_legendary_actions (
        creature_id integer,
        legendary_action_id integer,
        CONSTRAINT fk_creature_id FOREIGN KEY (creature_id) REFERENCES creatures(creature_id),
        CONSTRAINT fk_legendary_action_id FOREIGN KEY (legendary_action_id) REFERENCES legendary_actions(legendary_action_id)
);

COMMIT;
ROLLBACK;

DROP TABLE IF EXISTS creatures_actions;
DROP TABLE IF EXISTS actions;
DROP TABLE IF EXISTS creatures_legendary_actions;
DROP TABLE IF EXISTS legendary_actions;
DROP TABLE IF EXISTS creatures_reactions;
DROP TABLE IF EXISTS reactions;
DROP TABLE IF EXISTS creatures_subtypes;
DROP TABLE IF EXISTS subtypes;
DROP TABLE IF EXISTS creatures_attributes;
DROP TABLE IF EXISTS attributes;
DROP TABLE IF EXISTS creatures;
DROP TABLE IF EXISTS types;
DROP TABLE IF EXISTS users;




