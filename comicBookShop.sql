DROP TABLE IF EXISTS stock;
DROP TABLE IF EXISTS comic_book;
DROP TABLE IF EXISTS paint;
DROP TABLE IF EXISTS dice;
DROP TABLE IF EXISTS rpg_type;
DROP TABLE IF EXISTS table_top_game_rules;
DROP TABLE IF EXISTS figurine;
DROP TABLE IF EXISTS store_inventory;
DROP TABLE IF EXISTS store;
DROP TABLE IF EXISTS inventory;

CREATE TABLE inventory (
        inventory_id serial,
        item_name varchar(64) NOT NULL,
        item_type varchar(64) NOT NULL,
        price money,
        CONSTRAINT pk_inventory_inventory_id PRIMARY KEY (inventory_id)
);

CREATE TABLE stock (
        time_stamp timestamp,
        inventory_id int NOT NULL,
        number_in_store int NOT NULL,
        shipment_ordered boolean NOT NULL,
        CONSTRAINT pk_stock_time_stamp PRIMARY KEY (time_stamp),
        CONSTRAINT fk_inventory FOREIGN KEY (inventory_id) REFERENCES inventory(inventory_id)
);

CREATE TABLE comic_book (
        comic_id serial,
        item_type varchar(64) NOT NULL,
        publication varchar(64) NOT NULL,
        artist varchar(64) NOT NULL,
        release_date timestamp,
        CONSTRAINT pk_comic_book_comic_id PRIMARY KEY (comic_id),
        CONSTRAINT fk_inventory FOREIGN KEY (item_type) REFERENCES inventory(item_type)
);

CREATE TABLE paint (
        paint_id serial,
        item_type varchar(64) NOT NULL,
        color varchar(64) NOT NULL,
        CONSTRAINT pk_paint_paint_id PRIMARY KEY (paint_id),
        CONSTRAINT fk_inventory FOREIGN KEY (item_type) REFERENCES inventory(item_type)
);

CREATE TABLE board_game (
        board_game_id serial,
        item_type varchar(64) NOT NULL,
        producer varchar(64) NOT NULL,
        number_of_players int NOT NULL,
        minimum_age int NOT NULL,
        CONSTRAINT pk_board_game_board_game_id PRIMARY KEY (board_game_id),
        CONSTRAINT fk_inventory FOREIGN KEY (item_type) REFERENCES inventory(item_type)
);

CREATE TABLE dice (
        dice_id serial,
        item_type varchar(64) NOT NULL,
        number_of_faces int NOT NULL,
        material varchar(64) NOT NULL,
        CONSTRAINT pk_dice_dice_id PRIMARY KEY (dice_id),
        CONSTRAINT fk_inventory FOREIGN KEY (item_type) REFERENCES inventory(item_type)
);

CREATE TABLE rpg_type (
        rpg_id serial,
        game_name varchar(64) NOT NULL,
        CONSTRAINT pk_rpg_type_rpg_id PRIMARY KEY (rpg_id)
);

CREATE TABLE table_top_game_rules (
        rules_id serial,
        item_type varchar(64) NOT NULL,
        rpg_id int NOT NULL,
        CONSTRAINT pk_table_top_game_rules_rules_id PRIMARY KEY (rules_id),
        CONSTRAINT fk_inventory FOREIGN KEY (item_type) REFERENCES inventory(item_type),
        CONSTRAINT fk_rpg_type FOREIGN KEY (rpg_id) REFERENCES rpg_type(rpg_id)
);

CREATE TABLE figurine (
        figurine_id serial,
        item_type varchar(64) NOT NULL,
        rpg_id int NOT NULL,
        CONSTRAINT pk_figurine_figurine_id PRIMARY KEY (figurine_id),
        CONSTRAINT fk_inventory FOREIGN KEY (item_type) REFERENCES inventory(item_type),
        CONSTRAINT fk_rpg_type FOREIGN KEY (rpg_id) REFERENCES rpg_type(rpg_id)
);

CREATE TABLE store (
        address varchar(64) NOT NULL,
        phone_number varchar(32),
        CONSTRAINT pk_store_address PRIMARY KEY (address)
);

CREATE TABLE store_inventory (
        address varchar(64) NOT NULL,
        inventory_id int NOT NULL,
        CONSTRAINT fk_inventory FOREIGN KEY (inventory_id) REFERENCES inventory(inventory_id),
        CONSTRAINT fk_store FOREIGN KEY (address) REFERENCES store(address)
);

